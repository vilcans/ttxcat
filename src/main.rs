mod cli;

use std::{
    fs::File,
    io::{BufRead, BufReader, Read},
    path::{Path, PathBuf},
    str::FromStr,
};

use base64::Engine;
use clap::Parser;
use cli::{Format, Opts};
use teletext::{CharacterSet, NationalOption, SpacingAttribute, State};
use termcolor::{Color, ColorSpec, StandardStream, WriteColor};

fn render(
    input: &[u8],
    out: &mut dyn WriteColor,
    character_set: CharacterSet,
    print_attributes: bool,
) -> std::io::Result<()> {
    for line in input.chunks(40) {
        let mut current_color = ColorSpec::new();
        let mut state = State::new();
        for byte in line {
            if print_attributes {
                if let Some(attr) = SpacingAttribute::from_byte(*byte) {
                    current_color = ColorSpec::new();
                    out.set_color(&current_color)?;
                    write!(out, "{:?}", attr)?;
                }
            }
            let (element, new_state) = state.next(*byte);
            let mut color = ColorSpec::new();
            color.set_fg(Some(term_color(element.style.foreground)));
            color.set_bg(Some(term_color(element.style.background)));
            if color != current_color {
                out.set_color(&color)?;
                current_color = color;
            }
            let c = teletext::to_char(&element, character_set).to_string();
            write!(out, "{c}")?;
            state = new_state;
        }
        out.set_color(&ColorSpec::new())?;
        writeln!(out)?;
    }
    Ok(())
}

fn term_color(color: u8) -> Color {
    match color {
        0 => Color::Black,
        1 => Color::Red,
        2 => Color::Green,
        3 => Color::Yellow,
        4 => Color::Blue,
        5 => Color::Magenta,
        6 => Color::Cyan,
        _ => Color::White,
    }
}

fn read_hex(file: Option<&Path>) -> anyhow::Result<Vec<u8>> {
    let lines: Box<dyn BufRead> = match file {
        Some(f) => Box::new(BufReader::new(File::open(f)?)),
        None => Box::new(std::io::stdin().lock()),
    };
    let mut result = Vec::new();
    for line in lines.lines() {
        let line = line?;
        let mut bytes = hex::decode(line)?;
        result.append(&mut bytes);
    }
    Ok(result)
}

fn read_raw(file: Option<&Path>) -> std::io::Result<Vec<u8>> {
    match file {
        Some(f) => std::fs::read(f),
        None => {
            let mut buffer = Vec::new();
            std::io::stdin().lock().read_to_end(&mut buffer)?;
            Ok(buffer)
        }
    }
}

/// Read data in the URL format used by edit.tf and zxnet.co.uk.
/// See [format description](https://github.com/rawles/edit.tf#all-frame-data-is-kept-in-the-url).
///
/// Examples of matching strings (truncated):
///
/// - `https://edit.tf/#0:QIEC...A`
/// - `https://zxnet.co.uk/teletext/editor/#0:QIEC...A:PS=0:RE=0:zx=BA2`
fn read_url(url: &str) -> anyhow::Result<Vec<u8>> {
    // Extract the anchor part of the URL, i.e. after the first #
    let anchor = url.split_once('#').map(|(_, data)| data).unwrap_or(url);

    let parts: Vec<_> = anchor.split(':').collect();
    let (_character_set, encoded) = if parts.len() == 1 {
        ("0", parts[0])
    } else {
        (parts[0], parts[1])
    };

    let decoded = base64::engine::general_purpose::URL_SAFE_NO_PAD.decode(encoded)?;
    let bytes: Vec<u8> = (0..)
        .map_while(|index| {
            let bit = index * 7;
            let i = bit >> 3;
            let Some(b0) = decoded.get(i) else {
                return None;
            };
            let b1 = decoded.get(i + 1).unwrap_or(&0);
            let v = ((*b0 as u16) << 8) | (*b1 as u16);
            let shift = bit & 7;
            let v = v >> (9 - shift);
            Some(v as u8 & 0x7f)
        })
        .collect();
    Ok(bytes)
}

fn main() -> anyhow::Result<()> {
    let opts = Opts::parse();
    let mut stdout = StandardStream::stdout(termcolor::ColorChoice::Auto);

    let mut print = |input: Option<String>| -> anyhow::Result<()> {
        let teletext_data = get_teletext(input, opts.format)?;
        let character_set = CharacterSet::Latin(NationalOption::English);
        render(
            &teletext_data,
            &mut stdout,
            character_set,
            opts.show_attributes,
        )?;
        Ok(())
    };

    if opts.input.is_empty() {
        print(None)?;
    } else {
        for input in &opts.input {
            print(Some(input.clone()))?;
        }
    }
    Ok(())
}

/// Get Teletext data from the input.
fn get_teletext(input: Option<String>, format: Option<Format>) -> Result<Vec<u8>, anyhow::Error> {
    let input_file = || -> anyhow::Result<Option<PathBuf>> {
        match &input {
            None => Ok(None),
            Some(s) => Ok(Some(PathBuf::from_str(s)?)),
        }
    };
    let format = format.unwrap_or_else(|| match &input {
        Some(name) => {
            if name.starts_with("http:") || name.starts_with("https:") {
                Format::Url
            } else if name.ends_with(".hex") {
                Format::Hex
            } else {
                Format::Raw
            }
        }
        None => Format::Raw,
    });
    Ok(match format {
        Format::Raw => read_raw(input_file()?.as_deref())?,
        Format::Hex => read_hex(input_file()?.as_deref())?,
        Format::Url => read_url(&input.unwrap_or_default())?,
    })
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parse_edit_tf_url() {
        let data = read_url("https://edit.tf/#0:QIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAYQce6BAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgTX7d1AnTqlXv7bupsOLHky5s-jTq17Nu7fw48ufTr27oECBAgQIECBAgQIECBAgTIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECA").unwrap();
        assert_eq!(data.len(), 40 * 25);
        assert_eq!(data[0], 0x20);
    }

    #[test]
    fn simplest_url() {
        let data = read_url("0:AA").unwrap();
        assert_eq!(data, [0, 0]);
    }

    #[test]
    fn simplest_url_without_colon() {
        let data = read_url("AA").unwrap();
        assert_eq!(data, [0, 0]);
    }

    #[test]
    fn base64_decoding_and_grouping_by_7_bits() {
        // Characters:        1      2     3       4      0
        // Groups of 7:       .......-------.......-------.......
        // Value to encode:   00000010000010000001100001000000000
        // Groups of 6:       ......------......------......------
        // Base64 char:       0=A   32=g  32=g  24=Y  16=Q  0=A
        let data = read_url("#0:AggYQA").unwrap();
        assert_eq!(data, [1, 2, 3, 4, 0]);
    }

    #[test]
    fn url_with_additional_data() {
        let data = read_url("#0:BBBB:foo=42:bar=4711").unwrap();
        assert_eq!(data.len(), 4);
        assert_eq!(data, [2, 4, 8, 16]);
    }
}
