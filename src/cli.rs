use clap::{Parser, ValueEnum};

#[derive(Copy, Clone, ValueEnum)]
pub enum Format {
    /// Teletext characters as raw bytes
    Raw,
    /// Teletext characters as raw data in hexadecimal
    Hex,
    /// Encoding used by edit.tf: either only the base64 encoded characters, or a complete URL
    Url,
}

/// Print Teletext pages on the standard output.
///
/// Read the INPUT files given and prints their contents.
/// With no INPUT, read standard input.
///
/// Also supports a URL in the format used by the online Teletext editor edit.tf.
///
/// In the url format, INPUT is a URL that contains all the data; there is no network traffic.
///
/// Unless the --format option is given, guess the format depending on INPUT.
/// If INPUT starts with http: or https:, assumes --format=url.
/// If INPUT ends with .hex, assumes --format=hex.
/// Otherwise, assumes --format=raw.
#[derive(Parser)]
#[clap(version)]
pub struct Opts {
    /// Format of the input.
    #[clap(long, short)]
    pub format: Option<Format>,

    #[clap(long, short = 's')]
    /// Print the attributes as text (e.g. for debugging)
    pub show_attributes: bool,

    /// Filenames or, for the URL format, URLs.
    pub input: Vec<String>,
}
