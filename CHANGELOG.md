# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Unreleased

## 0.1.1 - 2023-09-08

### Fixed

- Links and alt text in readme.

## 0.1.0 - 2023-09-06

### Added

- Print contents from file or URL.
