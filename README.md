# ttxcat

A command-line application that prints a [Teletext](https://en.wikipedia.org/wiki/Teletext]) page to the console.

Maps the Teletext characters to Unicode and color control sequences.

## Examples

### Show file contents

Print a teletext page stored in a binary file:

![ttxcat ~/teletext-examples/snail.bin](pictures/snail.png)

Source: [zxnet.co.uk](https://zxnet.co.uk/teletext/gallery/index.php?gallery=gallery11)

### URL format

You can also read the page data from a URL to an [online Teletext editor](https://zxnet.co.uk/teletext/editor/).
Note that the data is encoded in the URL. There is no network traffic.

![ttxcat https://zxnet.co.uk/teletext/editor/#0:QIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECAt_____________________________________________y3_____________________9ugQIEa_________________Lf_____________________0qBAgQIEW______________8t__________________79OT1IECBAgKcOBZGrX________y3_______________-_Rl0DBAgUYExT1zdr__zgWRq9____Lf______________05dAgQIECBwhKev_9_v-c3a_784Fka8t____________9-hLr16UygQNkBT0_X__vRkrX__nNf_-9C3___________v0JdAgQIECBsgKYP_XggRI1-35wZq1_zmvLf_________9-hLoECBAgQNkCApq_____786cEH__9-cEa8t__________0JdAgQIECB0gQICiL__________________S3________-lLoOCBAgQOkCBAgQFES9f_______________Lf_______25dAgQIECBQawKFiJGUQIECDhwMo0aNev3___8t______-_Ql0CBAgQIEJpCgKIOCxYjRo0CBAixOECBAZRIyy9evXr05dAgQIECA0iUIEBRwjQIEBRAgaoECDUXWLOHDKlJfPnz48LoMDAygQIDSRAgKaEKDEgUokaBKgKINSBAXQOkCAl_QoEaEugQIEBpBw4IEBTQhQIFCBSgQqWJRAgbl06BBhcLCX9gg4MC6ZA2QGkSBBpNFGqBAg0IECBAg0rEyMur3r1ixYkJf____9L6ECJBoQGkDo0USoECBsgKIECBsX1IECBAg0IECAl______qXD_C7Y0wKKFmRzwWJkCBAoWJi7BBlQIECBBlQICX_______UuwQNTSVAgQGkCBAwQIECBAgQKUCLIgQIECBGgJL16NGjQF0CJCiWMEChQaQMEClAgQIECpggLIC6NAgQIECAsgQIECA0gwcECBAgQIC5pBlQIECAsgYF0SEkZQIECDh4-fyxrgoWJEaNAgQIECBAgQFzSLIwLIEJIygQIEGDh8_f_____LIECBAgQIECBAgQGsHBQsRIyRlAgQIMHD58___________8:zx=BA2:X280=00007F0010111F70670AA303E5009DFCCC333F777F7FF777FF7F7FFDDD101016:PS=0:RE=0](pictures/train.png)

 (Source: [Teletext editor at zxnet.co.uk](https://zxnet.co.uk/teletext/editor/#0:QIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECBAgQIECAt_____________________________________________y3_____________________9ugQIEa_________________Lf_____________________0qBAgQIEW______________8t__________________79OT1IECBAgKcOBZGrX________y3_______________-_Rl0DBAgUYExT1zdr__zgWRq9____Lf______________05dAgQIECBwhKev_9_v-c3a_784Fka8t____________9-hLr16UygQNkBT0_X__vRkrX__nNf_-9C3___________v0JdAgQIECBsgKYP_XggRI1-35wZq1_zmvLf_________9-hLoECBAgQNkCApq_____786cEH__9-cEa8t__________0JdAgQIECB0gQICiL__________________S3________-lLoOCBAgQOkCBAgQFES9f_______________Lf_______25dAgQIECBQawKFiJGUQIECDhwMo0aNev3___8t______-_Ql0CBAgQIEJpCgKIOCxYjRo0CBAixOECBAZRIyy9evXr05dAgQIECA0iUIEBRwjQIEBRAgaoECDUXWLOHDKlJfPnz48LoMDAygQIDSRAgKaEKDEgUokaBKgKINSBAXQOkCAl_QoEaEugQIEBpBw4IEBTQhQIFCBSgQqWJRAgbl06BBhcLCX9gg4MC6ZA2QGkSBBpNFGqBAg0IECBAg0rEyMur3r1ixYkJf____9L6ECJBoQGkDo0USoECBsgKIECBsX1IECBAg0IECAl______qXD_C7Y0wKKFmRzwWJkCBAoWJi7BBlQIECBBlQICX_______UuwQNTSVAgQGkCBAwQIECBAgQKUCLIgQIECBGgJL16NGjQF0CJCiWMEChQaQMEClAgQIECpggLIC6NAgQIECAsgQIECA0gwcECBAgQIC5pBlQIECAsgYF0SEkZQIECDh4-fyxrgoWJEaNAgQIECBAgQFzSLIwLIEJIygQIEGDh8_f_____LIECBAgQIECBAgQGsHBQsRIyRlAgQIMHD58___________8:zx=BA2:X280=00007F0010111F70670AA303E5009DFCCC333F777F7FF777FF7F7FFDDD101016:PS=0:RE=0))

### Test page

Engineering test page (source: [MODE 7 Teletext Test Pages](https://mdfs.net/System/Teletext/Samples/TestPage.htm)). Note that characters are mapped from the English character subset to Unicode:

![ttxcat ~/teletext-examples/testpg25](pictures/testpg25.png)

## Show attributes

To see how a page is made, you can print the raw control codes known as "spacing attributes":

![ttxcat --show-attributes ~/teletext-examples/trucklecrag.bin](pictures/trucklecrag.png)

Source: [zxnet.co.uk](https://zxnet.co.uk/teletext/gallery/index.php?gallery=gallery1)

## Installation

With Cargo:

```bash
cargo install ttxcat
```

## Features

The current version of ttxcat:

* Understands all of the attributes in the level 1 Teletext specification.
* Accepts control codes for changing the background and foreground colors.
* Maps mosaic graphics to the corresponding Unicode characters.
* Maps alphanumeric characters according to the national subset (English).

Missing features:

* Cannot print separated mosaic graphics as there are no Unicode characters for them.
* Does not handle double-height characters.
* Does not handle blinking characters.
* Shows parts of the page that are supposed to be concealed by default.

## Usage

Full usage is provided by the `--help` flag:

```
$ ttxcat --help
Print Teletext pages on the standard output.

Read the INPUT files given and prints their contents. With no INPUT, read standard input.

Also supports a URL in the format used by the online Teletext editor edit.tf.

In the url format, INPUT is a URL that contains all the data; there is no network traffic.

Unless the --format option is given, guess the format depending on INPUT. If INPUT starts with http: or https:, assumes --format=url. If INPUT ends with .hex, assumes --format=hex. Otherwise, assumes --format=raw.

Usage: ttxcat [OPTIONS] [INPUT]...

Arguments:
  [INPUT]...
          Filenames or, for the URL format, URLs

Options:
  -f, --format <FORMAT>
          Format of the input

          Possible values:
          - raw: Teletext characters as raw bytes
          - hex: Teletext characters as raw data in hexadecimal
          - url: Encoding used by edit.tf: either only the base64 encoded characters, or a complete URL

  -s, --show-attributes
          Print the attributes as text (e.g. for debugging)

  -h, --help
          Print help (see a summary with '-h')

  -V, --version
          Print version
```
