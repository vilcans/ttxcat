name := "ttxcat"

# Create binaries for Linux and Windows
binaries: linux windows

# Create Linux binary
linux: (build "x86_64-unknown-linux-gnu")
    ls -lh target/x86_64-unknown-linux-gnu/release/{{name}}

# Create Windows binary
windows: (build "x86_64-pc-windows-gnu")
    ls -lh target/x86_64-pc-windows-gnu/release/{{name}}.exe

# Invoke cargo build for a specific target
build target:
    rustup target add {{target}}
    cargo build --release --target={{target}}

